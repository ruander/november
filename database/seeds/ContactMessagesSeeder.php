<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ContactMessagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('contact_messages')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'contact_message' => $faker->sentences('5', true),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

}
