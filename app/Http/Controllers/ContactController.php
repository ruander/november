<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactMessage as CM;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //összes contact message kilistázása
        $contact_messages = CM::all();
        return view('admin.contact-messages.index', compact('contact_messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.contact-messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactFormRequest $request) {
      $cm = CM::create($request->all());
      
      return redirect()->to(route('contact-messages'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
         $cm = CM::find($id);
         
         return view('admin.contact-messages.update',compact('cm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactFormRequest $request, $id) {
        $cm = CM::find($id);
        $cm->update($request->all());
        //vagy
        //$cm = CM::updateOrCreate([ezen egyezések alapján],$request->all()); ->de jelen esetben nem túl szerencsés mivel nincs elsődleges kulcs adatunk az id-n kivül tehát az ->update itt sokkal jobb mivel eleve egy modosito űrlapról érkezünk tehát biztos hogy létezik az id (urlben is benne van)
//        $cm = CM::updateOrCreate(['id'=>$id],$request->all());
        //vagy static-where és update is alkalmazható
//        $cm = CM::where(['id'=>$id])->update($request->except('_token'));
        return redirect()->to(route('contact-messages'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $cm = CM::find($id);
        if($cm){
            $cm->delete();
        }
        return redirect()->back();
    }

}
