<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('kapcsolat', 'PageController@contact')->name('contact');
//post on küldött adatok feldolgozásának routolása
Route::post('kapcsolat', 'PageController@processContactForm');

Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
    //contact-messages ->példa crud és routegroup
    Route::prefix('contact-messages')->group(function() {
         Route::get('delete/{id}','ContactController@destroy');  
        Route::get('/', 'ContactController@index')->name('contact-messages');
        Route::get('new', 'ContactController@create')->name('contact-messages-create');//form
        Route::post('new', 'ContactController@store')->name('contact-messages-store');//feldolgozás
       //update
        Route::get('edit/{id}', 'ContactController@edit')->name('contact-messages-edit');//form
        Route::post('edit/{id}', 'ContactController@update')->name('contact-messages-update');
    });
});
Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
