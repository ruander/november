@extends('layouts.master')

@section('content')
#error handling
@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
</ul>
@endif
#messages
@if(Session::has('status'))
    <div class="alert alert-success">
        {{Session::get('status')}}
    </div>
@endif

{!! Form::open(['autocomplete'=>'off']) !!}
<div class="form-group">
    {!! Form::label('email','Adja meg email címét') !!}
    {!! Form::email('email',old('email'),['class' => 'form-control','placeholder'=>'john@doe.com']) !!}

    {!! Form::label('name','Adja meg nevét') !!}
    {!! Form::text('name',null,['class' => 'form-control','placeholder'=>'John Doe']) !!}

    {!! Form::label('contact_message','Írjon üzenetet') !!}
    {!! Form::textarea('contact_message',null,['class' => 'form-control','placeholder'=>'üzenet...']) !!}





</div>
{!! Form::submit('Gyí',['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
@endsection